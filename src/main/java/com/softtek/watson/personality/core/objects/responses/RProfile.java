package com.softtek.watson.personality.core.objects.responses;

import java.util.ArrayList;
import java.util.List;

public class RProfile {
	public String processedLanguage;
	
	public int wordCount;
	
	public List<RTrait> personality;
	
	public List<RTrait> needs;
	
	public List<RTrait> values;
	
	public RProfile()
	{
		this.personality = new ArrayList<>();
		this.needs = new ArrayList<>();
		this.values = new ArrayList<>();
	}
	
	public static String convertLanguage(String lan)
	{
		if (lan.equals("ar"))
		{
			return "Arabic";
		}
		else if (lan.equals("en"))
		{
			return "English";
		}
		else if (lan.equals("es"))
		{
			return "Spanish";
		}
		else if (lan.equals("ja"))
		{
			return "Japanese";
		}
		else if (lan.equals("ko"))
		{
			return "Korean";
		}
		else
		{
			return "";
		}
	}

	
}
