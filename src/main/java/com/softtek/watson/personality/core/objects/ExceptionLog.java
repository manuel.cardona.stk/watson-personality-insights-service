package com.softtek.watson.personality.core.objects;

import com.softtek.watson.personality.core.components.APIErrorLogInterface;

public class ExceptionLog {
	
	private String message;
	
	private String service;
	
	public ExceptionLog(String message)
	{
		this.message = message;
		this.service = APIErrorLogInterface.serviceName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}
	
}
