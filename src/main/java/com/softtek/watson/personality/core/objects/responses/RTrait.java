package com.softtek.watson.personality.core.objects.responses;

public class RTrait {
	
	public String name;
	
	public double percentile;
	
	public RTrait(String name, double percentile)
	{
		this.name = name;
		this.percentile = percentile;
		this.percentile = this.percentile * 100;
		this.percentile = Math.floor(this.percentile);
	}
}
