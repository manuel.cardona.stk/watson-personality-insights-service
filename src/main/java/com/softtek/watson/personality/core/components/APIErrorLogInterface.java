package com.softtek.watson.personality.core.components;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.softtek.watson.personality.core.objects.ExceptionLog;
import com.softtek.watson.personality.core.objects.TransactionLog;

public class APIErrorLogInterface {

	public static final String serviceName = "watson.personality.insights";
	
	private static final String urlBaseErrorManager = "https://coe-logging.herokuapp.com";
	
	private static final String urlException = "/logException";
	
	private static final String urlTransaction = "/logTransaction";
	
	public static String logException(ExceptionLog elog)
	{
		RestTemplate template = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("message", elog.getMessage());
		map.add("service", elog.getService());
		
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		
		String url = urlBaseErrorManager + urlException;
		ResponseEntity<String> responseEntity = template.postForEntity(url, request, String.class);
		System.out.println(responseEntity.getBody());
		return responseEntity.getBody();
	}
	
	public static String logTransaction(TransactionLog tlog)
	{
		RestTemplate template = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		
		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		map.add("message", tlog.getMessage());
		map.add("status", tlog.getStatus());
		map.add("service", tlog.getService());
		
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		
		String url = urlBaseErrorManager + urlTransaction;
		ResponseEntity<String> responseEntity = template.postForEntity(url, request, String.class);
		System.out.println(responseEntity.getBody());
		return responseEntity.getBody();
	}
}
