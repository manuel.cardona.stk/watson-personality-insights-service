package com.softtek.watson.personality.core.components;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.personality_insights.v3.PersonalityInsights;
import com.ibm.watson.personality_insights.v3.model.Profile;
import com.ibm.watson.personality_insights.v3.model.ProfileOptions;
import com.softtek.watson.personality.core.objects.ExceptionLog;

public class APIWatsonInterface {
	
	public static Profile getProfile(String txtContent)
	{
		Authenticator authenticator = new IamAuthenticator("E8rrcohbCEdoo4ksnUWZufe4qFDwHynSv_cXI62G-2A2");
		PersonalityInsights service = new PersonalityInsights("2017-10-13", authenticator);

		String text = txtContent;

		ProfileOptions options = new ProfileOptions.Builder()
		  .text(text)
		  .build();
		
		Profile profile = null;
		try
		{
			profile = service.profile(options).execute().getResult();
		}
		catch(Exception exc)
		{
			APIErrorLogInterface.logException(new ExceptionLog(exc.getMessage()));
		}
		
		return profile;
	}
	
}
