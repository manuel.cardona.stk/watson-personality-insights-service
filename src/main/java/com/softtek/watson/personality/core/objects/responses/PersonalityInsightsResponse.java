package com.softtek.watson.personality.core.objects.responses;

import org.json.JSONArray;
import org.json.JSONObject;

import com.ibm.watson.personality_insights.v3.model.Profile;
import com.ibm.watson.personality_insights.v3.model.Trait;

public class PersonalityInsightsResponse {
	
	private Profile profile;
	
	private PersonalityInsightsResponse.ResponseStatus status;
	
	public enum ResponseStatus {
		OK,
		NOT_RIGHT_LENGTH,
		CONNECTION_API_CHATBOT_ERROR,
		CONNECTION_API_WATSON_ERROR,
		CONNECTION_API_WATSON_NULL
	}
	
	public PersonalityInsightsResponse(PersonalityInsightsResponse.ResponseStatus status)
	{
		this.status = status;
	}
	
	public PersonalityInsightsResponse(Profile profile, PersonalityInsightsResponse.ResponseStatus status)
	{
		this.profile = profile;
		this.status = status;
	}
	
	public JSONObject parse()
	{
		JSONObject json = new JSONObject();
		
		putProcessedLanguage(this.profile, json);
		
		putWordCount(this.profile, json);
		
		putPersonality(this.profile, json);
		
		putNeeds(this.profile, json);
		
		putValues(this.profile, json);
		
		putStatus(json);
		
		return json;
	}
	
	private void putProcessedLanguage(Profile profile, JSONObject json)
	{
		if (this.status.equals(PersonalityInsightsResponse.ResponseStatus.OK))
		{
			json.put("processed_language", RProfile.convertLanguage(profile.getProcessedLanguage()));	
		}
		else
		{
			json.put("processed_language",  "");
		}
	}
	
	private void putWordCount(Profile profile, JSONObject json)
	{
		if (this.status.equals(PersonalityInsightsResponse.ResponseStatus.OK))
		{
			json.put("word_count", profile.getWordCount().intValue());
		}
		else
		{
			json.put("word_count",  "");
		}
	}
	
	private void putPersonality(Profile profile, JSONObject json)
	{
		// Obtain personality
		JSONArray personality = new JSONArray();
		if (this.status.equals(PersonalityInsightsResponse.ResponseStatus.OK))
		{
			for (Trait el : profile.getPersonality())
			{
				JSONObject item = new JSONObject();
				item.put("name", el.getName());
				item.put("percentile", Math.floor(el.getPercentile() * 100));
				personality.put(item);
			}
			for (Trait el : profile.getPersonality())
			{
				for (Trait ele : el.getChildren())
				{
					JSONObject item = new JSONObject();
					item.put("name", ele.getName());
					item.put("percentile", Math.floor(ele.getPercentile() * 100));
					personality.put(item);
				}
			}
		}
		json.put("personality", personality);
	}
	
	private void putNeeds(Profile profile, JSONObject json)
	{
		// Obtain needs
		JSONArray needs = new JSONArray();
		if (this.status.equals(PersonalityInsightsResponse.ResponseStatus.OK))
		{
			for (Trait el : profile.getNeeds())
			{
				JSONObject item = new JSONObject();
				item.put("name", el.getName());
				item.put("percentile", Math.floor(el.getPercentile() * 100));
				needs.put(item);
			}
		}
		json.put("needs", needs);
	}
	
	private void putValues(Profile profile, JSONObject json)
	{
		// Obtain values
		JSONArray values = new JSONArray();
		if (this.status.equals(PersonalityInsightsResponse.ResponseStatus.OK))
		{
			for (Trait el : profile.getValues())
			{
				JSONObject item = new JSONObject();
				item.put("name", el.getName());
				item.put("percentile", Math.floor(el.getPercentile() * 100));
				values.put(item);
			}
		}
		json.put("values", values);
	}

	private void putStatus(JSONObject json)
	{
		json.put("status", this.status.toString());
	}
}
