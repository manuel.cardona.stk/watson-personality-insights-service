package com.softtek.watson.personality.core.objects;

import com.softtek.watson.personality.core.components.APIErrorLogInterface;

public class TransactionLog {
	
	private String message;
	
	private String status;
	
	private String service;
	
	public TransactionLog(String message, String status)
	{
		this.message = message;
		this.status = status;
		this.service = APIErrorLogInterface.serviceName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}
	
}
