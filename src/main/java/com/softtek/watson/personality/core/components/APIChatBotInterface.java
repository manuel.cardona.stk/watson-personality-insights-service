package com.softtek.watson.personality.core.components;

import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class APIChatBotInterface {

	private static final String urlChatBot = "https://chatbotwatsonspr.herokuapp.com/chat/recordedConversation";
	
	public static String getChat()
	{
		RestTemplate template = new RestTemplate();
		
		ResponseEntity<String> response = template.getForEntity(urlChatBot, String.class);
		
		JSONObject responseJson = new JSONObject(response.getBody());
		return responseJson.getString("conversation");
	}
}
