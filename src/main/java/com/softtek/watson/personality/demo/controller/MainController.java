package com.softtek.watson.personality.demo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.ibm.watson.personality_insights.v3.model.Profile;
import com.ibm.watson.personality_insights.v3.model.Trait;
import com.softtek.watson.personality.core.components.APIChatBotInterface;
import com.softtek.watson.personality.core.components.APIErrorLogInterface;
import com.softtek.watson.personality.core.components.APIWatsonInterface;
import com.softtek.watson.personality.core.components.StringCleaner;
import com.softtek.watson.personality.core.objects.ExceptionLog;
import com.softtek.watson.personality.core.objects.TransactionLog;
import com.softtek.watson.personality.core.objects.responses.PersonalityInsightsResponse;
import com.softtek.watson.personality.core.objects.responses.RProfile;
import com.softtek.watson.personality.core.objects.responses.RTrait;

import java.util.StringTokenizer;

import org.json.JSONArray;
import org.json.JSONObject;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class MainController {

	@RequestMapping(value = "/proccess-text", method = RequestMethod.POST, produces = "application/json")
	public String getPersonalityInsights()
	{
		// Consume la API del chat
		String textChat = "";
		JSONObject response = new JSONObject();
		Profile profile = null;
		
		try
		{
			APIErrorLogInterface.logTransaction(new TransactionLog("Enviando petición al API de ChatBot", "Sending"));
			textChat = APIChatBotInterface.getChat();
			textChat = textChat.replace("|", "");
		} catch(Exception exc)
		{
			APIErrorLogInterface.logException(new ExceptionLog(exc.getMessage()));
			response = new PersonalityInsightsResponse(PersonalityInsightsResponse.ResponseStatus.CONNECTION_API_CHATBOT_ERROR).parse();
			return response.toString();
		}
		
		textChat = StringCleaner.removeWhiteSpaces(textChat);
		StringTokenizer st = new StringTokenizer(textChat);
		
		if (st.countTokens() <= 150)
		{
			APIErrorLogInterface.logTransaction(new TransactionLog("Enviando JSON de respuesta: Entrada con longitud insuficiente", "Returning"));
			response = new PersonalityInsightsResponse(PersonalityInsightsResponse.ResponseStatus.NOT_RIGHT_LENGTH).parse();
			return response.toString();
		}
		
		// Consume la API de Watson
		try
		{
			APIErrorLogInterface.logTransaction(new TransactionLog("Enviando petición al API de Watson", "Sending"));
			profile = APIWatsonInterface.getProfile(textChat);
		} catch (Exception exc)
		{
			APIErrorLogInterface.logException(new ExceptionLog(exc.getMessage()));
			response = new PersonalityInsightsResponse(PersonalityInsightsResponse.ResponseStatus.CONNECTION_API_WATSON_ERROR).parse();
			return response.toString();
		}
		
		if (profile != null)
		{
			response = new PersonalityInsightsResponse(profile, PersonalityInsightsResponse.ResponseStatus.OK).parse();
			return response.toString();
		}
		else
		{
			APIErrorLogInterface.logException(new ExceptionLog("El objeto Profile viene nulo"));
			response = new PersonalityInsightsResponse(PersonalityInsightsResponse.ResponseStatus.CONNECTION_API_WATSON_NULL).parse();
			return response.toString();
		}
	}
	
	@PostMapping("/send-transaction-log")
	public String sendErrorLog(@RequestParam("message-input") String message, @RequestParam("status-input") String status)
	{
		TransactionLog tlog = new TransactionLog(message, status);
		return APIErrorLogInterface.logTransaction(tlog);
	}
	
	@PostMapping("/send-exception-log")
	public String sendExceptionLog(@RequestParam("message-input") String message)
	{
		ExceptionLog elog = new ExceptionLog(message);
		return APIErrorLogInterface.logException(elog);
	}
	
	@GetMapping("/probe-get")
	public String probeGet()
	{
		return "{ 'mensaje' : 'Hola' }";
	}
}
