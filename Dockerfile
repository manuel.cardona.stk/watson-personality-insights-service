FROM openjdk:8
VOLUME /tmp
EXPOSE 7070
ADD ./target/watson.personality.demo-0.0.1-SNAPSHOT.jar watson-personality.jar
ENTRYPOINT ["java", "-jar", "/watson-personality.jar"]